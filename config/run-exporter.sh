#!/bin/sh

EXPORTER_PORT=${EXPORTER_PORT:-9113}

if test "$DEBUG"; then
    set -x
fi

exec python -u /usr/src/app/backups_exporter.py
