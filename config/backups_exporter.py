#!/usr/bin/env python

import os

if os.environ.get('EXPORTER_PORT') is not None:
    bind_port = os.environ.get('EXPORTER_PORT')
else:
    bind_port = 9113
if os.environ.get('BACKUPS_ROOT') is not None:
    backups_root = os.environ.get('BACKUPS_ROOT')
else:
    backups_root = '/backups'
if os.environ.get('DEBUG') is not None:
    do_debug = True
else:
    do_debug = False

if os.path.exists(backups_root) == False:
    print("Invalid BACKUPS_ROOT - path does not exist")
    exit(1)

import re
import subprocess
import time
from datetime import datetime
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily, REGISTRY

def convertJobNameToDate(name):
    try:
        return int(datetime.strptime(str(name), '%Y%m%d%H%M%S').strftime('%s'))
    except ValueError:
        return -1

def getBackupJobs():
    return next(os.walk(backups_root))[1]

def getDiskBytes():
    df = subprocess.Popen(["df", "--block-size=1", backups_root], stdout = subprocess.PIPE)
    output = df.communicate()[0].decode('utf-8')
    device, size, used, available, percent, mountpoint = output.split("\n")[1].split()
    return { 'size': size, 'used': used, 'available': available }

def getDiskInodes():
    df = subprocess.Popen(["df", "-i", backups_root], stdout = subprocess.PIPE)
    output = df.communicate()[0].decode('utf-8')
    device, size, used, available, percent, mountpoint = output.split("\n")[1].split()
    return { 'size': size, 'used': used, 'available': available }

def getRuns(target):
    checkpath = '%s/%s' % (backups_root, target)
    return next(os.walk(checkpath))[1]

class BackupsCollector(object):
    def collect(self):
        dfbytes  = getDiskBytes()
        dfinodes = getDiskInodes()
        if do_debug:
            print(dfbytes)
        yield GaugeMetricFamily('backup_filesystem_free_bytes',
                                'Backups Remaining Storage Space',
                                int(dfbytes['available']))
        yield GaugeMetricFamily('backup_filesystem_total_bytes',
                                'Backups Total Storage Space',
                                int(dfbytes['size']))
        yield GaugeMetricFamily('backup_filesystem_free_inodes',
                                'Backups Remaining Storage Inodes',
                                int(dfinodes['available']))
        yield GaugeMetricFamily('backup_filesystem_total_inodes',
                                'Backups Total Storage Inodes',
                                int(dfinodes['size']))

        jobs               = getBackupJobs()
        prom_failed_count  = CounterMetricFamily(
                                'backup_job_failed_count',
                                'Total Failed Backup Jobs',
                                labels=["jobname"])
        prom_last_failed   = GaugeMetricFamily(
                                'backup_job_last_failed_timestamp_seconds',
                                'Last Failed Backup Job date',
                                labels=["jobname"])
        prom_last_success  = GaugeMetricFamily(
                                'backup_job_last_successful_timestamp_seconds',
                                'Last Successful Backup Job date',
                                labels=["jobname"])
        prom_success_count = CounterMetricFamily(
                                'backup_job_successful_count',
                                'Total Successful Backup Jobs',
                                labels=["jobname"])
        for job in jobs:
            if re.match(r'^\.', job):
                continue
            elif re.match(r'lost.*found', job):
                continue
            hasRuns = getRuns(job)
            last_success = 0
            last_failure = 0
            success_count = 0
            failure_count = 0
            for c in hasRuns:
                if re.match(r'^\d+$', c):
                    if int(c) > last_success:
                        last_success = int(c)
                    success_count = success_count + 1
                else:
                    fl = re.search('^(\d+).failed$', c)
                    if fl:
                        if int(fl.group(1)) > last_failure:
                            last_failure = int(fl.group(1))
                        failure_count = failure_count + 1
            prom_last_failed.add_metric([job], convertJobNameToDate(last_failure))
            prom_last_success.add_metric([job], convertJobNameToDate(last_success))
            prom_failed_count.add_metric([job], failure_count)
            prom_success_count.add_metric([job], success_count)
            if do_debug:
                print("%s: failed=%d success=%d" % (job, failure_count, success_count))
        yield prom_last_failed
        yield prom_last_success
        yield prom_failed_count
        yield prom_success_count

if __name__ == "__main__":
    REGISTRY.register(BackupsCollector())
    start_http_server(int(bind_port))
    print("Exporter started - listening on :%s" % bind_port)
    while True:
        time.sleep(1)
