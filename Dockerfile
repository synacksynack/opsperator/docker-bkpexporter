FROM docker.io/python:3-slim-buster

# Backups Exporter image for OpenShift Origin

LABEL io.k8s.description="Kubernetes Backups Prometheus Exporter Image." \
      io.k8s.display-name="Kubernetes Backups Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,backups" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-bkpexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0"

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /usr/src/app /config
WORKDIR /usr/src/app
COPY config/* /usr/src/app/

RUN set -x \
    && mv run-exporter.sh / \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && apt-get -y install curl \
    && pip install --no-cache-dir -r requirements.txt \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "/run-exporter.sh" ]
USER 1001
