# k8s Prometheus Backups Exporter

Build with:

```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|   Variable name  |    Description            | Default       |
| :--------------- | ------------------------- | ------------- |
|  `BACKUPS_ROOT`  | Backups Path              | `/backups`    |
|  `EXPORTER_PORT` | Backups Exporter Port     | `9113`        |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     |
| :------------------ | ------------------------------- |
|  `/backups`         | Path hosting Kubernetes Backups |
